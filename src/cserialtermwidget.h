/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#pragma once

#include <QPlainTextEdit>
#include <QSerialPort>
#include <QWidget>

class ConsoleText;

namespace Ui {
class CSerialTermWidget;
}

class CSerialTermWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CSerialTermWidget(QWidget *parent = nullptr);
    ~CSerialTermWidget();

    QString title();

    bool isConnected();
    void disconnectPort();

signals:
    void titleChanged();

private slots:
    void on_serialPorts_currentIndexChanged(int index);
    void on_baudRate_currentIndexChanged(int index);
    void on_serialPorts_currentTextChanged(const QString &arg1);
    void on_toggleConnect_clicked();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::CSerialTermWidget *ui;
    QSerialPort *m_serial;
    bool mIsConnected = false;

    void connectPort();
    void handleError(QSerialPort::SerialPortError error);
    void readData();
    void writeData(const QByteArray &data);
};

class ConsoleText : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit ConsoleText(QWidget *parent = nullptr);

signals:
    void inputData(const QByteArray &data);

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void contextMenuEvent(QContextMenuEvent *event) override;
};
